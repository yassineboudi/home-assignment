#!usr/bin/python

import os
import torch
from tomatodetector import TomatoDetector
import argparse
from buildmodel import BuildModel


def has_tomatoes(image_path, model):

    classifier = TomatoDetector(model)
    return classifier.predict_image(image_path)


def options():

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='Commands')

    train_parser = subparsers.add_parser(
        'train', help='Train model based on ResNet')
    detect_parser = subparsers.add_parser(
        'detect', help='Given an image as input, detects if image food has tomato or not')

    train_parser.add_argument('label', type=str,
                              help='Label mapping', action='store')
    train_parser.add_argument('annotation', type=str,
                              help='Image annotations', action='store')
    train_parser.add_argument('dir', type=str,
                              help='Image folder', action='store')
    train_parser.add_argument('output', type=str,
                              help='Output path', action='store')
    train_parser.add_argument(
        '--epoch', type=int, help='Number of epoch', action='store')
    train_parser.add_argument('--plot', help='save plot', action='store_true')

    detect_parser.add_argument(
        'input', type=str, help='Input image', action='store')
    detect_parser.add_argument(
        '--model', type=str, default='models/model_best.pth', action='store', help='Model to use')

    args = vars(parser.parse_args())

    return args


def main():

    args = options()

    if 'label' in args:
        data_dir = args['dir']
        mapping_file_path = args['label']
        annotation_file_path = args['annotation']
        path_model = args['output']
        save_plot = False
        nb_epoch = 25

        if args['epoch'] is not None:
            nb_epoch = args['epoch']

        if args['plot'] is not None:
            save_plot = True

        if os.path.exists(mapping_file_path) and os.path.exists(annotation_file_path) and os.path.isdir(data_dir):
            model = BuildModel(data_dir, mapping_file_path,
                               annotation_file_path)
            model.train_model(nb_epoch)
            model.save_model(path_model)
            if save_plot:
                model.save_plots()
        else:
            print("Files or directory doesn't exist")
            exit(0)

    elif 'input' in args:

        img_test_path = args['input']
        if args['model'] is not None:
            model_path = os.path.abspath(args['model'])
            if not os.path.exists(model_path):
                print("Model not found")
                exit(0)

        if os.path.exists(img_test_path):
            model = torch.load(model_path)
            print("The image: ", img_test_path, " has tomato: ",
                  has_tomatoes(img_test_path, model))
        else:
            print("Files or directory doesn't exist")
            exit(0)
    else:
        print("Arguments not found")
        exit(0)


if __name__ == '__main__':
    main()
