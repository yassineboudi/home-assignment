import os
import os.path
import torch.utils.data as data
import torch
from PIL import Image
from torchvision import transforms
from torch.autograd import Variable


class TomatoDetector():

    def __init__(self, model_ft):

        self.device = torch.device(
            "cuda" if torch.cuda.is_available() else "cpu")

        self.model = model_ft
        self.model.eval()

    def predict_image(self, image_path):

        data_transforms = transforms.Compose([
            transforms.Resize(size=256),
            transforms.CenterCrop(size=224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [
                0.229, 0.224, 0.225])
        ])

        image_tensor = data_transforms(Image.open(image_path)).float()
        image_tensor = image_tensor.unsqueeze_(0)
        input = Variable(image_tensor)
        input = input.to(self.device)
        output = self.model(input)
        index = output.data.cpu().numpy().argmax()
        return True if index == 1 else False
