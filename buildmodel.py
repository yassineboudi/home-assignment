from __future__ import print_function, division

import torch
import os
import os.path

import torch.utils.data as data
import matplotlib.pyplot as plt
import torch.nn.functional as F
from torchvision.datasets.folder import default_loader
import torch.nn as nn
from PIL import Image
from torchvision import datasets, models, transforms
from tomatodataset import TomatoDataset
import time
import torch.optim as optim
from torch.optim import lr_scheduler
import copy
import warnings

warnings.filterwarnings("ignore")


class BuildModel():

    def __init__(self, data_dir, mapping_file_path, annotation_file_path):

        self.data_transforms = {
            'train':
            transforms.Compose([
                transforms.RandomResizedCrop(size=256, scale=(0.8, 1.0)),
                transforms.RandomRotation(degrees=15),
                transforms.ColorJitter(),
                transforms.RandomHorizontalFlip(),
                transforms.CenterCrop(size=224),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406],
                                     [0.229, 0.224, 0.225])
            ]),
            'valid':
            transforms.Compose([
                transforms.Resize(size=256),
                transforms.CenterCrop(size=224),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [
                                     0.229, 0.224, 0.225])
            ]),
        }

        self.device = torch.device(
            "cuda") if torch.cuda.is_available() else torch.device("cpu")

        dataset_tomato = TomatoDataset(
            mapping_file_path, annotation_file_path, data_dir, transforms=self.data_transforms['train'])

        split_size_train = 0.6

        n_train = int(dataset_tomato.len * split_size_train)
        n_val = int((dataset_tomato.len - n_train))
        self.dataset_sizes = {
            'train': n_train, 'val': n_val}
        self.dataloaders = dataset_tomato.split_dataset(split_size_train)

        self.classes = dataset_tomato.classes

        self.epoch_losses = {'train': [], 'val': []}
        self.epoch_accs = {'train': [], 'val': []}

        self.model = models.resnet18(pretrained=True)

    def train_model(self, num_epochs=25):

        since = time.time()

        num_ftrs = self.model.fc.in_features

        self.model.fc = nn.Linear(num_ftrs, 2)

        self.model = self.model.to(self.device)

        criterion = nn.CrossEntropyLoss()

        optimizer = optim.SGD(
            self.model.parameters(), lr=0.0001, momentum=0.9)

        scheduler = lr_scheduler.StepLR(
            optimizer, step_size=7, gamma=0.1)

        best_model_wts = copy.deepcopy(self.model.state_dict())
        best_acc = 0.0

        for epoch in range(num_epochs):
            print('Epoch {}/{}'.format(epoch, num_epochs - 1))
            print('-' * 10)

            for phase in ['train', 'val']:
                if phase == 'train':
                    self.model.train()
                else:
                    self.model.eval()

                running_loss = 0.0
                running_corrects = 0

                for inputs, labels in self.dataloaders[phase]:
                    inputs = inputs.to(self.device)
                    labels = labels.to(self.device)

                    optimizer.zero_grad()

                    with torch.set_grad_enabled(phase == 'train'):
                        outputs = self.model(inputs)
                        _, preds = torch.max(outputs, 1)
                        loss = criterion(outputs, labels)

                        if phase == 'train':
                            loss.backward()
                            optimizer.step()

                    running_loss += loss.item() * inputs.size(0)
                    running_corrects += torch.sum(preds == labels.data)
                if phase == 'train':
                    scheduler.step()

                epoch_loss = running_loss / self.dataset_sizes[phase]
                epoch_acc = running_corrects.double() / \
                    self.dataset_sizes[phase]

                self.epoch_losses[phase].append(epoch_loss)
                self.epoch_accs[phase].append(epoch_acc)

                print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                    phase, epoch_loss, epoch_acc))

                if phase == 'val' and epoch_acc > best_acc:
                    best_acc = epoch_acc
                    best_model_wts = copy.deepcopy(self.model.state_dict())

            print()
        time_elapsed = time.time() - since
        print('Training complete in {:.0f}m {:.0f}s'.format(
            time_elapsed // 60, time_elapsed % 60))
        print('Best val Acc: {:4f}'.format(best_acc))

        self.model.load_state_dict(best_model_wts)

    def save_model(self, path_model):
        torch.save(self.model, path_model)

    def save_plots(self):
        plt.ion()

        vars_plots = {"accuracy": [self.epoch_accs, "plots/Accuracy.png"],
                      "loss": [self.epoch_losses, "plots/Losses.png"]}

        for idx in vars_plots:
            plt.figure()
            plot_train = vars_plots[idx]
            plot_dict = plot_train[0]
            plot_file_title = plot_train[1]
            plt.plot(plot_dict['train'], label='Training')
            plt.plot(plot_dict['val'], label='Validation')
            plt.legend(frameon=False)
            plt.savefig(plot_file_title)
