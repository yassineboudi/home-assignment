import os
import os.path

import torch.utils.data as data
import torch
from torchvision.datasets.folder import default_loader
import csv
import json
from torch.utils import data
from PIL import Image
from torchvision import transforms


class TomatoDataset(data.Dataset):

    def __init__(self, mapping_file_path, annotation_file_path, root_dir, transforms=None):

        self.root_dir = root_dir
        self.mapping_file_path = mapping_file_path
        self.annotation_file_path = annotation_file_path
        self.labels = self.__get_labels()

        self.classes_label = list(self.labels.items())
        self.len = len(self.labels)

        classes = ["no_tomato", "tomato"]
        self.class_to_idx = {classes[i]: i for i in range(len(classes))}
        self.classes = classes

        self.transform = transforms

    def __get_labels(self):

        label_map = {}
        with open(self.mapping_file_path, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                key = row['labelling_id'].strip()
                val = row['labelling_name_en'].strip()
                label_map[key] = val

        with open(self.annotation_file_path) as json_file:
            img_annotation = json.load(json_file)

        img_labels = {}
        for key in img_annotation:
            img_dict_item = img_annotation[key]
            img_labels[key] = 0
            for item in img_dict_item:
                label_str = label_map[item['id']].lower()
                if "tomato" in label_str:
                    img_labels[key] = 1

        return img_labels

    def split_dataset(self, train_size):

        n_train = int(self.len * train_size)
        n_val = int((self.len - n_train))

        train_set, val_set = data.random_split(
            self, (n_train, n_val))

        train_loader = torch.utils.data.DataLoader(
            train_set, batch_size=4, shuffle=True, num_workers=4)

        val_loader = torch.utils.data.DataLoader(
            val_set, batch_size=4, shuffle=True, num_workers=4)

        dataloaders = {'train': train_loader, 'val': val_loader}
        return dataloaders

    def __getitem__(self, idx):

        img_name = os.path.join(self.root_dir,
                                self.classes_label[idx][0])
        image = self.transform(Image.open(img_name))
        label = self.classes_label[idx][1]

        return image, label

    def __len__(self):
        return len(self.labels)
