# TomatoClassifier

### Prerequisites

OS tested:

- Ubuntu 16.04

Dependencies:

- pytorch 1.3.1

## Usage

The program has two modes:

1. train : train a model with given images and classes

2. detect: given a an image as input, returns if the image contains tomato or not

```

python tomtool.py <commands> [args]

```

### Train command

```

python tomtool.py train <label_mapping> <image_annotation> <image_dir> <output_path_model> <epoch> <plot>

```

#### Inputs:

- `<label_mapping>` : csv file contains names of each class.

- `<image_annotation>` : json file image annotations .

- `<image_dir>` : image folder that represents dataset.

- `<--epoch>` [Optional] : number of epoch. If it's not set, the default value is 25.

- `<--plot>` [Optional]: Whether to save losses and accuracy plots or not.

#### Outputs:

- `<output_path_model>` : File name to save trained model.

#### Example:

```

python tomtool.py train res/label_mapping.csv res/img_annotations.json data/ models/model.pth --plot

```

### Detect command

```

python tomtool.py detect <input_image> <model> <result>

```

#### Input(s):

- `<input_image>` : path to the input image.

- `<--model>` [Optional]: path to the pytorch model.If it's not set, the default model will be chosen.

#### Outputs:

- `<result>` : True if the image contains tomatoes, false otherwise .

#### Example:

```

python tomtool.py detect test/0_tomato.jpg

```

## Solution

1. Data preparation:

As a first step for this task, is to extract the relevant information used for image classification. The labels and classes were provided the format of a mapping class name and an image annotation that contains various information like bounding boxes which is useful for segmentation tasks. For the task demanded , I filtered the given labels into two categories:

- Has tomato: class with the word 'tomato' on the english name label in the file label_mapping.csv

- Has no tomato: with no mention of the word 'tomato'

Then at the end, combining the information of has no tomato/tomato with the image annotations, I created a single label file which goes like this:

<image_name><tomato:yes|no>

the details of this part is on the class: tomatodataset.py

2. Training:

Based on the size of the dataset provided, I decided to build my model using the technique of transfer learning.

The pretrained model is resnet101 trained on ImageNet. The network resnet18 has a good performance on the ImageNet. This dataset contains images of tomatoes, and it encourages me to use it.

the details of this part is on the class: buildmodel.py

## Refrences

https://pytorch.org

http://www.image-net.org

## Authors

Yassine Boudi
